//
//  ForgetViewController.swift
//  Student Union
//
//  Created by brst on 21/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import  FirebaseAuth
class ForgetViewController: UIViewController ,UITextFieldDelegate{
    @IBOutlet weak var ForgetPassword: UITextField!
   
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBAction func back(_ sender: Any) {
         _ = self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func forgotPassword(_ sender: Any) {
    
        if self.ForgetPassword.text == "" {
            let alertController = UIAlertController(title: "Oops!", message: "Please enter an email.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            FIRAuth.auth()?.sendPasswordReset(withEmail: self.ForgetPassword.text!, completion: { (error) in
                
                var title = ""
                var message = ""
                
                if error != nil {
                    title = "Error!"
                    message = (error?.localizedDescription)!
                } else {
                    title = "Success!"
                    message = "Reset password link successfully sent in your email Id.Thanks"
                    self.ForgetPassword.text = ""
                }
                
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            })
        }
    
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForgetViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

         self.forgotPasswordButton.layer.cornerRadius = 10
        ForgetPassword.text = ""
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 60, height: 30))
        ForgetPassword.leftView=paddingView;
        ForgetPassword.leftViewMode = UITextFieldViewMode.always

       
    }
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

    

}
