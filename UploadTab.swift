//
//  UploadTab.swift
//  Student Union
//
//  Created by mrinal khullar on 2/28/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
class UploadTab: UIViewController,UITextViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    @IBOutlet var loaderView: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var labelCount: UILabel!
    @IBOutlet var descLabel: UILabel!
    
    let picker = UIImagePickerController()
    var isImage = Bool()
    var postImg: UIImage?
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var myTextView: UITextView!
    
    
    @IBOutlet var postButton: UIButton!
    @IBAction func post(_ sender: Any) {
        
        if (postImg != nil) {
             UIApplication.shared.beginIgnoringInteractionEvents()
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.loaderView.isHidden = false
            let Alert=UIAlertController.init(title: title, message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            Alert.addAction(okAction)
            let uid = FIRAuth.auth()!.currentUser!.uid
            let ref = FIRDatabase.database().reference()
            let storage = FIRStorage.storage().reference(forURL: "gs://student-union-1ab1e.appspot.com")
            
            let key = ref.child("posts").childByAutoId().key
            
            
            
            let imageRef = storage.child("posts").child(uid).child("\(key).jpg")
            
            
            let data = UIImageJPEGRepresentation(self.imageView.image!, 0.6)
            let uploadTask = imageRef.put(data!, metadata: nil) { (metadata, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    Alert.title = "Alert"
                    Alert.message = error?.localizedDescription
                    self.present(Alert, animated: true, completion: nil)
                    return
                }
                
                imageRef.downloadURL(completion: { (url, error) in
                    if let url = url {
                        let feed = ["userID" : uid,
                                    "text" : self.myTextView.text,
                                    "pathToImage" : url.absoluteString,
                                    "likes" : 0,
                                    "author" : FIRAuth.auth()!.currentUser!.displayName!,
                                    "postID" : key] as [String : Any]
                        
                        let postFeed = ["\(key)" : feed]
                        
                        ref.child("posts").updateChildValues(postFeed)
                        
                        
                        self.dismiss(animated: true, completion: nil)
                        
                        self.myTextView.text = ""
                        self.labelCount.text = "0/400"
                        self.imageView.image = UIImage(named:"image")!
                        self.postImg = nil
                        self.descLabel.isHidden = false
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                        self.loaderView.isHidden = true
                         UIApplication.shared.endIgnoringInteractionEvents()
                        Alert.title = "Alert"
                        Alert.message = "Sucessfully posted !"
                        self.present(Alert, animated: true, completion: nil)
                        
                        
                    }
                })
                
            }
            
            uploadTask.resume()
        }
            
        else  if postImg == nil && myTextView.text == ""{
            
            let Alert=UIAlertController.init(title: title, message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            Alert.addAction(okAction)
            Alert.title = "Alert"
            Alert.message = "Please write description or add picture to post."
            self.present(Alert, animated: true, completion: nil)
            
        }
        else {
            
             UIApplication.shared.beginIgnoringInteractionEvents()
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.loaderView.isHidden = false
            let Alert=UIAlertController.init(title: title, message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            Alert.addAction(okAction)
            let uid = FIRAuth.auth()!.currentUser!.uid
            let ref = FIRDatabase.database().reference()
            
            let key = ref.child("posts").childByAutoId().key
            
            
            
            
            let feed = ["userID" : uid,
                        "text" : self.myTextView.text,
                        "pathToImage" : "",
                        "likes" : 0,
                        "author" : FIRAuth.auth()!.currentUser!.displayName!,
                        "postID" : key] as [String : Any]
            
            let postFeed = ["\(key)" : feed]
            
            ref.child("posts").updateChildValues(postFeed)
            
            
            self.dismiss(animated: true, completion: nil)
            
            self.myTextView.text = ""
            self.labelCount.text = "0/400"
            self.imageView.image = UIImage(named:"image")!
            postImg = nil
            self.descLabel.isHidden = false
            
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.loaderView.isHidden = true
             UIApplication.shared.endIgnoringInteractionEvents()
            Alert.title = "Alert"
            Alert.message = "Sucessfully posted !"
            self.present(Alert, animated: true, completion: nil)
            
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isImage == false {
            self.myTextView.text = ""
            self.labelCount.text = "0/400"
            self.imageView.image = UIImage(named:"image")!
            postImg = nil
            self.descLabel.isHidden = false
        } else {
            isImage = false
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.isHidden = true
        self.loaderView.isHidden = true
        self.loaderView.layer.cornerRadius = 10
        myTextView.setContentOffset(CGPoint.zero, animated: false)
        
        
        self.postButton.layer.cornerRadius = 10
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        
        
    }
    func dismissKeyboard() {
        
        if myTextView.text == "" {
            self.descLabel.isHidden = false
        }
        
        view.endEditing(true)
    }
    @IBAction func gallery(_ sender: Any) {
        
        isImage = true
        showActionSheet1()
        
        
    }
    func camera1()
    {
        
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .camera
        self.present(picker, animated: true, completion: { _ in })
    }
    
    
    
    
    func photoLibrary1()
    {
        
        
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: { _ in })
    }
    
    
    
    
    func showActionSheet1() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }

    
    @IBAction func Camera(_ sender: Any) {
        
        isImage = true
        
        showActionSheet()
        
        
    }
    func camera()
    {
        
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .camera
        self.present(picker, animated: true, completion: { _ in })
    }
    
    
    
    
    func photoLibrary()
    {
        
        
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: { _ in })
    }
    
    
    
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    

    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = image
            postImg = image
            
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    func textViewDidChange(_ textView: UITextView){
        
        
        
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool{
        self.descLabel.isHidden = true
        return true
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //self.descLabel.isHidden = true
        if(text == "\n")
        {
            if myTextView.text == "" {
                self.descLabel.isHidden = false
            }
            textView.endEditing(true)
            return false
        }
        else
        {
            
            
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.characters.count
            if numberOfChars < 401 {
                labelCount.text = "\(numberOfChars)/400"
            }
            return numberOfChars < 401
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
}
