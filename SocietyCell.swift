//
//  SocietyCell.swift
//  Student Union
//
//  Created by mrinal khullar on 2/24/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class SocietyCell: UITableViewCell {
   
    @IBOutlet var greyImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var lastNameLabel: UILabel!
    
    var userID: String!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
