//
//  SocietyTabBar.swift
//  Student Union
//
//  Created by mrinal khullar on 2/24/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class SocietyTabBar: UITabBarController ,UITabBarControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        if let items = self.tabBar.items {
            
            
            let height = self.tabBar.bounds.height
            
            
            let numItems = CGFloat(items.count)
            let itemSize = CGSize(
                width: tabBar.frame.width / numItems,
                height: tabBar.frame.height)
            
            for (index, _) in items.enumerated() {
                
               
                
                if index > 0 {
                    
                  
                    
                    let xPosition = itemSize.width * CGFloat(index)
                                      let separator = UIView(frame: CGRect(
                        x: xPosition, y: 0, width: 0.5, height: height))
                    separator.backgroundColor = UIColor.white
                    tabBar.insertSubview(separator, at: 1)
                }
            }
        }
        
        
    self.navigationController?.isNavigationBarHidden=true
      
        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 50)!], for: .normal)
         UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 50)!], for: .selected)
          UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.normal)
         UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.selected)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

}
