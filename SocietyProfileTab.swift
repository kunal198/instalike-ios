//
//  SocietyProfileTab.swift
//  Student Union
//
//  Created by mrinal khullar on 2/28/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class SocietyProfileTab: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var submit: UIButton!
    @IBAction func submitButton(_ sender: Any) {
    }
    @IBOutlet var confirmPasswordField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var lastNameField: UITextField!
    @IBOutlet var firstNameField: UITextField!
    @IBOutlet var pic: UIImageView!
    @IBOutlet var myLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
     let picker = UIImagePickerController()
   
    
    @IBAction func addPhoto(_ sender: Any) {
        showActionSheet()
        
    }
    func camera()
    {
        
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .camera
        self.present(picker, animated: true, completion: { _ in })
    }
    
    
    
    
    func photoLibrary()
    {
        
        
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: { _ in })
    }
    
    
    
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        
        
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = image
            self.pic.isHidden = true
            //self.addYourPhoto.isHidden = true
            //self.addButton.isHidden = true
        }
        self.dismiss(animated: true, completion: nil)
        
        
    

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstNameField.attributedPlaceholder = NSAttributedString(string:"First Name",
                                                             attributes:[NSForegroundColorAttributeName: UIColor.white])
        
        
        lastNameField.attributedPlaceholder = NSAttributedString(string:"Last Name",
                                                                  attributes:[NSForegroundColorAttributeName: UIColor.white])
        
        emailField.attributedPlaceholder = NSAttributedString(string:"E-mail",
                                                                  attributes:[NSForegroundColorAttributeName: UIColor.white])
        
        passwordField.attributedPlaceholder = NSAttributedString(string:"Password",
                                                                  attributes:[NSForegroundColorAttributeName: UIColor.white])
        
        confirmPasswordField.attributedPlaceholder = NSAttributedString(string:"Confirm Password",
                                                                  attributes:[NSForegroundColorAttributeName: UIColor.white])
        self.imageView.layer.cornerRadius =  10
        self.firstNameField.layer.cornerRadius = 10
        self.lastNameField.layer.cornerRadius = 10
        self.emailField.layer.cornerRadius = 10
        self.passwordField.layer.cornerRadius = 10
        self.confirmPasswordField.layer.cornerRadius = 10
        self.submit.layer.cornerRadius = 10
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 30))
        firstNameField.leftView=paddingView
        firstNameField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView1 = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 30))
        lastNameField.leftView=paddingView1
        lastNameField.leftViewMode = UITextFieldViewMode.always
        
        
        
        
        
        
        
        
        let paddingView2 = UIView(frame:CGRect(x: 0, y: 0, width: 60, height: 30))
        emailField.leftView=paddingView2
        emailField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView3 = UIView(frame:CGRect(x: 0, y: 0, width: 60, height: 30))
        passwordField.leftView=paddingView3
        passwordField.leftViewMode = UITextFieldViewMode.always
        
        
        let paddingView4 = UIView(frame:CGRect(x: 0, y: 0, width: 60, height: 30))
        confirmPasswordField.leftView=paddingView4
        confirmPasswordField.leftViewMode = UITextFieldViewMode.always
        
    }
    func dismissKeyboard() {
        
        view.endEditing(true)
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
        
    }
    class TextField: UITextField {
        
        let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5);
        
        override func textRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func editingRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
    }
    

   
}
