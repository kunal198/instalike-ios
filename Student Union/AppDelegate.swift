//
//  AppDelegate.swift
//  Student Union
//
//  Created by brst on 21/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var get = String()
    var  googleGets = String()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        if  ((FIRAuth.auth()?.currentUser) != nil) {
            if UserDefaults.standard.string(forKey: "Type") == nil{
                
            }
            else{
                get = UserDefaults.standard.string(forKey: "Type")!
                
                
                print(googleGets)
                if get == "Student"{
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarController")
                    let root = UINavigationController.init(rootViewController: initialViewController)
                    appDelegate.window?.rootViewController = root
                    appDelegate.window?.makeKeyAndVisible()
                    
                    
                    
                }
                if get == "Society"{
                    
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "SocietyTabBar")
                    let root = UINavigationController.init(rootViewController: initialViewController)
                    appDelegate.window?.rootViewController = root
                    appDelegate.window?.makeKeyAndVisible()
                }
                    
               
            }
            
          //  print(GIDSignIn().currentUser)
//            
//            let signIn = GIDSignIn.sharedInstance()
//            signIn?.scopes = ["https://www.googleapis.com/auth/plus.login","https://www.googleapis.com/auth/plus.me"]
//            
//            if ((signIn?.hasAuthInKeychain) != nil) {
//                print("Signed in");
//            } else {
//                print("Not signed in");
//            }
            
         //   if GIDSignIn().currentUser != nil {
                if UserDefaults.standard.string(forKey:"Typ") == nil {
                    
                }
                else{
                    googleGets = UserDefaults.standard.string(forKey:"Typ")!
                    if googleGets == "Student"{
                        
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarController")
                        let root = UINavigationController.init(rootViewController: initialViewController)
                        appDelegate.window?.rootViewController = root
                        appDelegate.window?.makeKeyAndVisible()
                        
                    }
                    else if googleGets == "Society"{
                        
                        
                        
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "SocietyTabBar")
                        let root = UINavigationController.init(rootViewController: initialViewController)
                        appDelegate.window?.rootViewController = root
                        appDelegate.window?.makeKeyAndVisible()
                        
                        
                    }
                }
                
                
                
          
            
            
            
        }
        
        
        
        
        
        
        
        
        
        return true
    }
    
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            if #available(iOS 9.0, *) {
                return GIDSignIn.sharedInstance().handle(url,
                                                         sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                         annotation: [:])
            } else {
                // Fallback on earlier versions
            }
            
            return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
        
    }
    
    //func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
    //
    //    if let error = error {
    //
    //        return
    //    }
    //
    //    guard let authentication = user.authentication else { return }
    //    let credential = FIRGoogleAuthProvider.credential(withIDToken: authentication.idToken,
    //                                                      accessToken: authentication.accessToken)
    //
    //}
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

