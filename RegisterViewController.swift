//
//  RegisterViewController.swift
//  Student Union
//
//  Created by brst on 21/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
class RegisterViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate{
     var type = String()
    let picker = UIImagePickerController()
    @IBOutlet var imageView: UIImageView!
    var userStorage: FIRStorageReference!
    var ref: FIRDatabaseReference!
    @IBOutlet var loader: UIActivityIndicatorView!
   
    @IBOutlet var myLabel: UILabel!
    @IBOutlet var myView: UIView!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var addYourPhoto: UILabel!
    @IBOutlet var pic: UIImageView!
    var userInfo: [String : Any] = [:]
    
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var societySelected: UIImageView!
    @IBOutlet weak var studentSelected: UIImageView!
    @IBOutlet weak var sSociety: UIButton!
    @IBOutlet weak var sStudent: UIButton!
   
    
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var lastName: UITextField!
    
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var EmailField: UITextField!
    
    var society = true
    @IBAction func society(_ sender: Any) {
       
        
         type = "Society"
        self.studentSelected.isHidden = true
        
        self.societySelected.isHidden = false
        

    }
    
    
    @IBAction func student(_ sender: Any) {
        
            type = "Student"
            self.studentSelected.isHidden = false
        
            self.societySelected.isHidden = true

            
        
        
    }
   
   
    
    
    
   
   
    
    
   
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myView.layer.cornerRadius = 10
        self.myView.isHidden = true
        self.myLabel.isHidden = true
        self.loader.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        type = "Society"
       
        
        
        
        self.imageView.layer.cornerRadius = 10
        
         firstName.text = ""
        lastName.text = ""
        EmailField.text = ""
        passwordField.text = ""
        confirmPasswordField.text = ""
         self.createAccountButton.layer.cornerRadius = 10
        
         self.studentSelected.isHidden = true
        
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 30))
        firstName.leftView=paddingView;
        firstName.leftViewMode = UITextFieldViewMode.always
        
        
        let paddingView1 = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 30))
        lastName.leftView=paddingView1;
        lastName.leftViewMode = UITextFieldViewMode.always
        
        
        let paddingView2 = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 30))
        EmailField.leftView=paddingView2;
        EmailField.leftViewMode = UITextFieldViewMode.always
        
        
        let paddingView3 = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 30))
        passwordField.leftView=paddingView3;
        passwordField.leftViewMode = UITextFieldViewMode.always
        
        
        let paddingView4 = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 30))
        confirmPasswordField.leftView=paddingView4;
        confirmPasswordField.leftViewMode = UITextFieldViewMode.always
        
        picker.delegate = self
        
        let storage = FIRStorage.storage().reference(forURL: "gs://student-union-1ab1e.appspot.com")
        
        ref = FIRDatabase.database().reference()
        userStorage = storage.child("users")
        
       
    }
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
       @IBAction func back(_ sender: Any) {
       _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func createAccount(_ sender: Any) {
        
      
        
        
        
            let Alert=UIAlertController.init(title: title, message: "", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        Alert.addAction(okAction)
        
        
        

           if firstName.text == ""{
                            Alert.title = "Alert"
                            Alert.message = "Please enter first name."
                            self.present(Alert, animated: true, completion: nil)
        
                        }
          else if lastName.text == "" {
            Alert.title = "Alert"
            Alert.message = "Please enter last name."
            self.present(Alert, animated: true, completion: nil)
            
            
         }
        
            else  if EmailField.text == "" {
                            Alert.title = "Alert"
                            Alert.message = "Please enter an email address."
                            self.present(Alert, animated: true, completion: nil)
        
                        }
        
            else if passwordField.text == ""{
                            Alert.title = "Alert"
                            Alert.message = "Please enter password."
                            self.present(Alert, animated: true, completion: nil)
                            
                            
                            
                            
                        }
            else if confirmPasswordField.text == ""{
            Alert.title = "Alert"
            Alert.message = "Please enter confirm password."
            self.present(Alert, animated: true, completion: nil)
            

            
         }
           else if passwordField.text != confirmPasswordField.text {
            
            
            Alert.title = "Alert"
            Alert.message = "Password does not match."
            self.present(Alert, animated: true, completion: nil)
            
            
           }
            
           
            
         else{
        
        
        FIRAuth.auth()?.createUser(withEmail: EmailField.text!, password: passwordField.text! , completion: { (user, error) in
            
            
            if let error = error {
                print(error.localizedDescription)

                
                
                
                Alert.title = "Alert"
                Alert.message = error.localizedDescription
                self.present(Alert, animated: true, completion: nil)
                

            }
            
            if let user = user {
                
                 self.myView.isHidden = false
                 self.myLabel.isHidden = false
                  self.loader.isHidden = false
                  self.loader.startAnimating()
                UIApplication.shared.beginIgnoringInteractionEvents()
                
                let changeRequest = FIRAuth.auth()!.currentUser!.profileChangeRequest()
                changeRequest.displayName = self.firstName.text!
                changeRequest.commitChanges(completion: nil)
                
                let imageRef = self.userStorage.child("\(user.uid).jpg")
                
                let data = UIImageJPEGRepresentation(self.imageView.image!, 0.5)
                
                let uploadTask = imageRef.put(data!, metadata: nil, completion: { (metadata, err) in
                    if err != nil {
                        print(err!.localizedDescription)
                    }
                    
                    imageRef.downloadURL(completion: { (url, er) in
                        if er != nil {
                            print(er!.localizedDescription)
                        }
                        
                        
                        if let url = url {
                            
                            
                            let name = "\(self.firstName.text!) \(self.lastName.text!)"
                            
                           self.userInfo = ["uid" : user.uid,
                                                            "name" : name,
                                                            "Type"    :  self.type ,
                                                            "urlToImage" : url.absoluteString]
                            
                            self.ref.child("users").child(user.uid).setValue(self.userInfo)
                            
                           
                            
                            
                         

                            
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
                         
                            self.navigationController?.pushViewController(vc, animated: true)
                             UIApplication.shared.endIgnoringInteractionEvents()
                            Alert.title = "Alert"
                            Alert.message = "Registered successfully."
                           
                            self.present(Alert, animated: true, completion: nil)

                            
                        }
                        
                    })
                    
                })
                
                uploadTask.resume()
                
            }
            
            
        })
        
        

        
        
        
        
        }
        
        
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if passwordField == textField || confirmPasswordField == textField || EmailField == textField {
      animateViewMoving(up: true, moveValue: 125)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
         if passwordField == textField || confirmPasswordField == textField || EmailField == textField{
        animateViewMoving(up: false, moveValue: 125)
        }
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
        
    }

    @IBAction func AddPhoto(_ sender: Any) {
        
        
        
         showActionSheet()
       
            
            }
    func camera()
    {
        
            var picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: { _ in })
        }
    
        
    
    
    func photoLibrary()
    {
        
        
            var picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: { _ in })
}



    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
        
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = image
            self.pic.isHidden = true
           self.addYourPhoto.isHidden = true
            //self.addButton.isHidden = true
        }
        self.dismiss(animated: true, completion: nil)

        
   }
    }
