//
//  TabBarController.swift
//  Student Union
//
//  Created by brst on 21/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let items = self.tabBar.items {
            
            
            
            let height = self.tabBar.bounds.height
            
            
            
            let numItems = CGFloat(items.count)
            let itemSize = CGSize(
                width: tabBar.frame.width / numItems,
                height: tabBar.frame.height)
            
            for (index, _) in items.enumerated() {
                
                
                
                if index > 0 {
                    
                    
                    
                    let xPosition = itemSize.width * CGFloat(index)
                    
                    
                    let separator = UIView(frame: CGRect(
                        x: xPosition, y: 0, width: 0.5, height: height))
                    separator.backgroundColor = UIColor.white
                    tabBar.insertSubview(separator, at: 1)
                }
            }
        }
    
    
    

      self.navigationController?.isNavigationBarHidden=true
        
        
        
//       UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 25)!], for: .normal)
//         UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 25)!], for: .selected)
        
      UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.normal)
         UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.selected)
      }
    
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    

}
