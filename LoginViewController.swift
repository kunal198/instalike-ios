//
//  LoginViewController.swift
//  Student Union
//
//  Created by brst on 21/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
class LoginViewController: UIViewController ,GIDSignInUIDelegate,GIDSignInDelegate , UITextFieldDelegate{
    @IBOutlet var googleButton: UIButton!
    var type = String()
    var gets = String()
    var googleGets = String()
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var get = [String:AnyObject]()
    var userInfo: [String : Any] = [:]

    var ref: FIRDatabaseReference!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
   
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        
      
        
        view.addGestureRecognizer(tap)
      
       self.activityIndicator.isHidden = true
          ref = FIRDatabase.database().reference()
        
        
        
        
        
        
        
        emailField.text = ""
        passwordField.text = ""
       self.loginButton.layer.cornerRadius = 10
        
        
        
        GIDSignIn.sharedInstance().uiDelegate = self
     
      
        self.navigationController?.navigationBar.isHidden=true
        
        
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 30))
        emailField.leftView=paddingView;
        emailField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView1 = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 30))
        passwordField.leftView=paddingView1
        passwordField.leftViewMode = UITextFieldViewMode.always
        
            }
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func loginPress(_ sender: Any) {
        

        let Alert=UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        Alert.addAction(okAction)
        
        
        
         
      
        
         if self.emailField.text == "" && self.passwordField.text == ""{
         Alert.title = "Alert"
         Alert.message = "Please enter an email address."
         self.present(Alert, animated: true, completion: nil)
         
         }
         
         else  if self.emailField.text == ""{
         Alert.title = "Alert"
         Alert.message = "Please enter email ID."
         self.present(Alert, animated: true, completion: nil)
         
         }
         
         else  if self.passwordField.text == ""{
         
         Alert.title = "Alert"
         Alert.message = "Please enter password."
         self.present(Alert, animated: true, completion: nil)
         
         
         }
         
        
         else {
        
         
        FIRAuth.auth()?.signIn(withEmail: emailField.text!, password: passwordField.text!, completion: { (user, error) in
            
            if let error = error {
                
                print(error.localizedDescription)
                
                let Alert=UIAlertController.init(title: self.title, message: "", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                Alert.addAction(okAction)
                
                Alert.title = "Alert"
                Alert.message = error.localizedDescription
                self.present(Alert, animated: true, completion: nil)
                
                
                
                }
            


            
            
            
            if let user = user {
                UIApplication.shared.beginIgnoringInteractionEvents()

                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            self.ref.child("users").child(user.uid).observe(.value, with: { (FIRUserInfo) in
                
            
                self.get = FIRUserInfo.value as? [String : AnyObject] ?? [:]
            
                 //let getEmail=self.get["Email"]
                // let getPassword=self.get["password"]
                 let   getType = self.get["Type"] as! String
                    UserDefaults.standard.set(getType, forKey: "Type")
                  self.gets = UserDefaults.standard.string(forKey: "Type")!
                  print(self.gets)
            
                UIApplication.shared.endIgnoringInteractionEvents()

            if getType == "Student"{
                    
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarController")
                    self.present(vc, animated: true, completion: nil)
                    }
            
                
        if getType == "Society" {
                    
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocietyTabBar")
                self.present(vc, animated: true, completion: nil)
                                        
                }
        

            })
        }
        })
        }
    }
    @IBAction func goRegister(_ sender: Any) {
        emailField.text = ""
        passwordField.text = ""
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)

    }
    @IBAction func goForgot(_ sender: Any) {
        emailField.text = ""
        passwordField.text = ""
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgetViewController") as! ForgetViewController
      self.navigationController?.pushViewController(secondViewController, animated: true)
        
        
    }
   
    @IBAction func googleButtton(_ sender: Any) {
        

            let optionMenu = UIAlertController(title: nil, message: "Choose Type", preferredStyle: .actionSheet)
           let Std = UIAlertAction(title: "Student", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.type = "Student"

            GIDSignIn.sharedInstance().signIn()
            GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
           GIDSignIn.sharedInstance().delegate = self
            
        
            
           })
        let Soc = UIAlertAction(title: "Society", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
           
            self.type = "Society"


                GIDSignIn.sharedInstance().signIn()
                GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
                GIDSignIn.sharedInstance().delegate = self

            
        })
        
        
        let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
          
        })
        
      
        
        optionMenu.addAction(Std)
        optionMenu.addAction(Soc)
        optionMenu.addAction(Cancel)
        
      
        
        
    self.present(optionMenu, animated: true, completion: nil)
        
       
    }
        
        
    
    
    
    
    
    
    
    func sign(_ googleSignIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        if let error = error {
            
            
            
            
            
            
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = FIRGoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)
        UIApplication.shared.beginIgnoringInteractionEvents()

        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        FIRAuth.auth()?.signIn(with: credential) { (user, error) in
            
            
            
            self.userInfo = ["uid" : user?.uid,
                             "name" : user?.displayName,
                             "Type"    :  self.type ,
                             "urlToImage" : user?.photoURL?.absoluteString]
            
            //self.ref.child("users").child((user?.uid)!).setValue(self.userInfo)
            
            
            self.ref.child("users").child((user?.uid)!).updateChildValues(self.userInfo)

            
            
            UserDefaults.standard.set(self.type, forKey: "Typ")
            self.googleGets = UserDefaults.standard.string(forKey:"Typ")!
            
            UIApplication.shared.endIgnoringInteractionEvents()

            if self.type == "Student" {
                  if GIDSignIn.sharedInstance().currentUser != nil{
                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                
                let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabBarController")
                let root = UINavigationController.init(rootViewController: initialViewController)
                appDelegate.window?.rootViewController = root
                appDelegate.window?.makeKeyAndVisible()
                }
            }
            if self.type == "Society"{
                
                
                if GIDSignIn.sharedInstance().currentUser != nil{
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    
                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "SocietyTabBar")
                    let root = UINavigationController.init(rootViewController: initialViewController)
                    appDelegate.window?.rootViewController = root
                    appDelegate.window?.makeKeyAndVisible()
                    
                    
                }

            
        }
        
        }
    
    
    }
   

   
    
    
  


    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }

    }
    class TextField: UITextField {
        
        let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5);
        
        override func textRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func editingRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
    }

}
