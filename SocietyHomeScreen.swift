//
//  SocietyHomeScreen.swift
//  Student Union
//
//  Created by brst on 21/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn
class SocietyHomeScreen: UIViewController ,GIDSignInUIDelegate{

    @IBAction func logout(_ sender: Any) {
        
        if GIDSignIn().currentUser == nil {
            //GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().signOut()
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate

            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationController")
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
            
            //  Logout(sender.self)
        }
        if FIRAuth.auth()?.currentUser != nil {
            do {
                try FIRAuth.auth()?.signOut()
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
                self.navigationController?.pushViewController(vc, animated: true)
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

      
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
