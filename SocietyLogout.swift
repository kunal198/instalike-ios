//
//  SocietyLogout.swift
//  Student Union
//
//  Created by mrinal khullar on 3/4/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn
class SocietyLogout: UIViewController, GIDSignInUIDelegate,UITabBarControllerDelegate,UITabBarDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
    }
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure want to logout ?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: {action in
            
            self.tabBarController?.selectedIndex = 0
            
        } ))
        
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { action in
            
            
            if GIDSignIn().currentUser == nil {
                //GIDSignIn.sharedInstance().delegate = self
                GIDSignIn.sharedInstance().signOut()
                UserDefaults.standard.set("", forKey: "Typ")
                UserDefaults.standard.set("", forKey: "Type")
                // UserDefaults.standard.string(forKey:"Typ")!
                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationController")
                appDelegate.window?.rootViewController = vc
                appDelegate.window?.makeKeyAndVisible()
                
                //  Logout(sender.self)
            }
            if FIRAuth.auth()?.currentUser == nil {
                do {
                    try FIRAuth.auth()?.signOut()
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            
            
            
            
            
        }))
        
        
        
        self.present(alert, animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
