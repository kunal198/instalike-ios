//
//  SocietyTab.swift
//  Student Union
//
//  Created by mrinal khullar on 2/24/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SDWebImage
import GoogleSignIn

class SocietyTab: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
     var resultDict = NSMutableDictionary()
     var firstnameArray = NSMutableArray()
     var secondnameArray = NSMutableArray()
     var image = NSMutableArray()
     var new = [[String:String]]()
     var users2 = [[String:String]]()
     var selectionArray = NSMutableArray()
    var results = NSMutableArray()
     var arr = String()
     var sortedResults: Any = [[String:String]]()
    
    class User: NSObject {
        
       // var userID: String!
        var firstName = NSMutableArray()
        var lastName = NSMutableArray()
        var imagePath = NSMutableArray()
        
    }

    var user = [User]()
    @IBOutlet var textField: UITextField!

@IBOutlet var SocietyTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
            self.textField.text = ""
        textField.attributedPlaceholder = NSAttributedString(string:"Search",
                                                                  attributes:[NSForegroundColorAttributeName: UIColor.white])
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 30))
       textField.leftView=paddingView;
       textField.leftViewMode = UITextFieldViewMode.always
        
        
              textField.delegate = self
             SocietyTableView.delegate = self
             SocietyTableView.dataSource = self
             retrieveUsers()
    }
    func sign(_ googleSignIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        if let error = error {
            
            
            
            
            
            
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = FIRGoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)
        FIRAuth.auth()?.signIn(with: credential) { (user, error) in
            
           let googleuser = user?.displayName
            
            
            
        }
        }
    @IBAction func Search(_ sender: Any) {
        
        
        if self.textField.text != "" {
            let resultPredicate = NSPredicate(format: "name contains[cd] %@", self.textField.text!)
            self.users2 = (new as! NSMutableArray).filtered(using: resultPredicate) as! [[String : String]]
            print(self.users2.count)
        } else {
        
            self.users2=self.new
        }
        
        SocietyTableView.reloadData()
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    class TextField: UITextField {
        
        let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5);
        
        override func textRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func editingRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
    }

    func retrieveUsers() {
        let ref = FIRDatabase.database().reference()
        
        ref.child("users").queryOrderedByKey().observeSingleEvent(of: .value, with: { snapshot in
            
            
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            for value in postDict {
                let dict1 = value.value as! NSDictionary
                if(dict1.value(forKey: "Type") as! String == "Society"){
                    self.users2.append(dict1 as! [String : String])
                   
           // var descriptor: NSSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
            //       var sortedResults: Any = self.users2.sorted(by: descriptor)
                    
                   // print(sortedResults)
                  
                    
                    
                    self.new.append(dict1 as! [String : String])
                }
            }
         
            
            self.SocietyTableView.reloadData()
        })
        ref.removeAllObservers()
        
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict1 = users2[indexPath.row] as NSDictionary
        if selectionArray.contains(dict1)
        {
            selectionArray.remove(dict1)
        } else
        {
            selectionArray.add(dict1)
        }
       // SocietyTableView.reloadData()
       SocietyTableView.reloadRows(at: [indexPath], with: .none)
    }
   

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return users2.count
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return 1
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell:SocietyCell = self.SocietyTableView.dequeueReusableCell(withIdentifier: "cell") as! SocietyCell

        cell.nameLabel.text=users2[indexPath.row]["name"]! as String
      //  cell.userImage.downloadImage(from: self.users2[indexPath.row]["urlToImage"])

        let imageStr = self.users2[indexPath.row]["urlToImage"]
        
        if imageStr == "" {
            
             cell.userImage.image = UIImage(named:"image")!
            
        }
        else{
       cell.userImage.sd_setImage(with: URL(string: imageStr!))
        
        }
        
        let dict1 = users2[indexPath.row] as NSDictionary
        if selectionArray.contains(dict1) {
            cell.greyImage.image = UIImage(named:"blue-btn")!
        } else {
            cell.greyImage.image = UIImage(named:"grey-btn")!
        }
        
        return cell
    }
}

